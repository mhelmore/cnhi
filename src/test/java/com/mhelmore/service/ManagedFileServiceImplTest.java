package com.mhelmore.service;


import com.mhelmore.domain.ManagedFile;
import com.mhelmore.enums.ManagedFileStatus;
import com.mhelmore.enums.ManagedFileType;
import com.mhelmore.persistence.ManagedFileBrokerStub;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;


public class ManagedFileServiceImplTest {
    public static final Long ANY_ORG_ID = -1L;
    private static final long ANY_FILE_ID = 1L;
    private static final String ANY_FILE_URI = "anyUri";
    private static final Long ANY_USER_ID = 4L;
    private static final Long ANY_FILE_SIZE = 1l;
    private static final String ANY_FILE_NAME1 = "anyFileName1";

    @Mock
    ManagedFileBrokerStub fileRepository;

    @InjectMocks
    ManagedFileServiceImpl serviceToTest;



    @BeforeEach void setUp(){
        MockitoAnnotations.initMocks(this);
        serviceToTest = new ManagedFileServiceImpl(fileRepository);

    }

    @Test
    public void listUserFiles_() {
        List<ManagedFile> files = new ArrayList<ManagedFile>();

        List<ManagedFile> actual = serviceToTest.listUserFiles(ANY_USER_ID);

        assertEquals(files, actual);
    }



    @Test
    public void testGetttingManagedFile() throws Exception {
        ManagedFile mockFile = mock(ManagedFile.class);

        when(mockFile.getId()).thenReturn(ANY_FILE_ID);
        when(fileRepository.findManagedFileById(ANY_FILE_ID)).thenReturn(mockFile);
        ManagedFile file = serviceToTest.getManageFile(ANY_FILE_ID);
        verify(fileRepository, atLeastOnce() ).findManagedFileById(ANY_FILE_ID);
        assertEquals((long)ANY_FILE_ID, (long)file.getId());
    }


    private ManagedFile createManagedFile(ManagedFileStatus fileStatus, ManagedFileType fileType) {
        ManagedFile managedFile = new ManagedFile();
        managedFile.setId(ANY_FILE_ID);
        managedFile.setFileStatus(fileStatus);
        managedFile.setFileType(fileType);
        managedFile.setFileSize(ANY_FILE_SIZE);
        managedFile.setUri(ANY_FILE_URI);
        managedFile.setFileName(ANY_FILE_NAME1);

        return managedFile;
    }

    private ManagedFile getManagedfileByName(String name) {
        ManagedFile managedFile = new ManagedFile();
        managedFile.setFileName(name);
        return managedFile;
    }

}