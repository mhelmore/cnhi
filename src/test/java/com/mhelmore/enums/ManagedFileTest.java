package com.mhelmore.enums;

import com.mhelmore.domain.ManagedFile;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;


class ManagedFileTest {

    ManagedFile managedFile;

    @BeforeEach
    void setUp() {
        managedFile = new ManagedFile();
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    public void testCheckManagedFileIsNotTransferableWhenFileTypeIsUnknown() throws Exception {

        managedFile.setFileType(ManagedFileType.UNKNOWN);

        assertFalse(managedFile.isTransferable());
    }

    @Test
    public void testCheckManagedFileIsNotTransferableWhenFileTypeIsInvalid() throws Exception {

        managedFile.setFileType(ManagedFileType.INVALID);

        assertFalse(managedFile.isTransferable());
    }

    @Test
    public void testCheckManagedFileIsNotTransferableWhenFileStatusIsEitherRCDOrSetUpNotPresent() throws Exception {

        managedFile.setFileStatus(ManagedFileStatus.NON_TRANSFERABLE);

        assertFalse(managedFile.isTransferable());
    }


    @Test
    public void testCheckManagedFileIsNotTransferableWhenFileStatusIsInvalidZipFile() throws Exception {

        managedFile.setFileStatus(ManagedFileStatus.INVALID_ZIP_FILE);

        assertFalse(managedFile.isTransferable());
    }

    @Test
    public void testCheckManagedFileIsTransferableWhenFileTypeIsValidAndStatusIsTransferable() throws Exception {

        managedFile.setFileStatus(ManagedFileStatus.TRANSFERABLE);
        managedFile.setFileType(ManagedFileType.SETUP);

        assertTrue(managedFile.isTransferable());

    }

    @Test
    public void testManagedFileIsNotInspectedWhenFileTypeIsUnknown() throws Exception {
        managedFile.setFileType(ManagedFileType.UNKNOWN);
        assertFalse(managedFile.isInspected());
    }

    @Test
    public void testManagedFileIsInspectedWhenFileTypeIsSetup() throws Exception {
        managedFile.setFileType(ManagedFileType.SETUP);
        assertTrue(managedFile.isInspected());
    }

    @Test public void shouldReturnTrueWhenStatusIsUploadPending() throws Exception {
        ManagedFile managedFile = new ManagedFile();
        managedFile.setFileStatus(ManagedFileStatus.UPLOAD_PENDING);
        assertTrue(managedFile.isUploadPendingStatus());
    }

    @Test public void shouldReturnFalseWhenStatusIsNotUploadPending() throws Exception {
        ManagedFile managedFile = new ManagedFile();
        managedFile.setFileStatus(ManagedFileStatus.NON_TRANSFERABLE);
        assertFalse(managedFile.isUploadPendingStatus());
    }
}