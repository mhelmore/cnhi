package com.mhelmore.enums;

public enum ManagedFileStatus {  TRANSFERABLE(1), INVALID_ZIP_FILE(2), NON_TRANSFERABLE(3), UPLOAD_PENDING(4);

    private int code;

    private ManagedFileStatus(int c) {
        code = c;
    }

    public int getCode() {
        return code;
    }


    public static ManagedFileStatus getStatusFromCode(int code) {
        ManagedFileStatus managedFileStatuses[] = ManagedFileStatus.values();
        for (ManagedFileStatus managedFileStatus : managedFileStatuses) {
            if (managedFileStatus.ordinal() == code) {
                return managedFileStatus;
            }
        }
        return null;
    }

    public static int getStatusOrdinalFromName(String name) {
        ManagedFileStatus managedFileStatuses[] = ManagedFileStatus.values();
        for (ManagedFileStatus managedFileStatus : managedFileStatuses) {
            if (managedFileStatus.name().equalsIgnoreCase(name)) {
                return managedFileStatus.ordinal();
            }
        }
        return -1;
    }
}
