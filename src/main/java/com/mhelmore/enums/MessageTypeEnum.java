package com.mhelmore.enums;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public enum MessageTypeEnum { SETUP_MESSAGE(1), COMMAND_MESSAGE(2), LOGGED_DATA_MESSAGE(3), OFFICE_FILE_MESSAGE(4);

    private int code;

    private MessageTypeEnum(int c){
        code = c;
    }

    public int getCode() {
        return code;
    }

    public static int getEnumValue(String strFileType) {
        return ManagedFileType.valueOf(strFileType).ordinal();
    }

    public static List<Integer> getOrdinalsAsList() {
        List<Integer> ordinalList = new ArrayList<Integer>();
        ManagedFileType managedFileTypes[] = ManagedFileType.values();
        for (ManagedFileType managedFileType : managedFileTypes) {
            ordinalList.add(managedFileType.ordinal());
        }
        return ordinalList;
    }

    public static ManagedFileType getTypeFromOrdinal(int ordinal) {
        ManagedFileType managedFileTypes[] = ManagedFileType.values();
        for (ManagedFileType managedFileType : managedFileTypes) {
            if (managedFileType.ordinal() == ordinal) {
                return managedFileType;
            }
        }
        return null;
    }

    public static List<ManagedFileType> getTypesAsList() {
        return Arrays.asList(ManagedFileType.values());
    }

    public static String getOrdinalAsArrayForOracleInClause(List<ManagedFileType> fileTypes) {
        StringBuffer ordinalsAsString = new StringBuffer();
        int i = 0;
        for (ManagedFileType fileType : fileTypes) {
            ordinalsAsString.append(fileType.ordinal());
            if (++i < fileTypes.size()) {
                ordinalsAsString.append(",");
            }
        }
        return ordinalsAsString.toString();
    }

}
