package com.mhelmore.enums;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public enum ManagedFileType {
    SETUP(MessageTypeEnum.SETUP_MESSAGE.getCode()),
    PRESCRIPTION(MessageTypeEnum.COMMAND_MESSAGE.getCode()),
    INVALID(1001),
    UNKNOWN(1002),
    DOC(MessageTypeEnum.LOGGED_DATA_MESSAGE.getCode()),
    JDBACKUP(208),
    HIC(2050),
    TIMBER(MessageTypeEnum.OFFICE_FILE_MESSAGE.getCode()),
    EXPORT(124),
    PERMISSION(000),
    WHATS_NEW(-1);


    private int code;

    private ManagedFileType(int c) {
        code = c;
    }

    public int getCode() {
        return code;
    }

    public static int getEnumValue(String strFileType) {
        return ManagedFileType.valueOf(strFileType).ordinal();
    }

    public static List<Integer> getOrdinalsAsList() {
        List<Integer> ordinalList = new ArrayList<Integer>();
        ManagedFileType managedFileTypes[] = ManagedFileType.values();
        for (ManagedFileType managedFileType : managedFileTypes) {
            ordinalList.add(managedFileType.ordinal());
        }
        return ordinalList;
    }

    public static ManagedFileType getTypeFromOrdinal(int ordinal) {
        ManagedFileType managedFileTypes[] = ManagedFileType.values();
        for (ManagedFileType managedFileType : managedFileTypes) {
            if (managedFileType.ordinal() == ordinal) {
                return managedFileType;
            }
        }
        return null;
    }

    public static List<ManagedFileType> getTypesAsList() {
        return Arrays.asList(ManagedFileType.values());
    }

    public static String getOrdinalAsArrayForOracleInClause(List<ManagedFileType> fileTypes) {
        StringBuffer ordinalsAsString = new StringBuffer();
        int i = 0;
        for (ManagedFileType fileType : fileTypes) {
            ordinalsAsString.append(fileType.ordinal());
            if (++i < fileTypes.size()) {
                ordinalsAsString.append(",");
            }
        }
        return ordinalsAsString.toString();
    }

}
