package com.mhelmore.domain;

import com.mhelmore.enums.ManagedFileStatus;
import com.mhelmore.enums.ManagedFileType;

import java.util.Calendar;



public class ManagedFile {

    private Long id;


    private String uri;

    private String fileName;

    private ManagedFileStatus fileStatus;

    private ManagedFileType fileType;

    private Calendar dateCreated;

    private Calendar dateModified;

    private Long fileSize;

    private Long organizationId;

    private Long userId;

    private String fileSource;



    private String csObjectId;



    public ManagedFile(Long id) {
        this();
        setId(id);
    }

    public ManagedFile() {
    }

    public String getCsObjectId() {
        return csObjectId;
    }

    public void setCsObjectId(String csObjectId) {
        this.csObjectId = csObjectId;
    }


    public String getUri() {
        return this.uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public ManagedFileStatus getFileStatus() {
        return this.fileStatus;
    }

    public void setFileStatus(ManagedFileStatus fileStatus) {
        this.fileStatus = fileStatus;
    }

    public ManagedFileType getFileType() {
        return this.fileType;
    }

    public void setFileType(ManagedFileType fileType) {
        this.fileType = fileType;
    }

    public Calendar getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Calendar dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Calendar getDateModified() {
        return dateModified;
    }

    public void setDateModified(Calendar dateModified) {
        this.dateModified = dateModified;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Long getFileSize() {
        return fileSize;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Id: ").append(getId()).append(", ");
        sb.append("Uri: ").append(getUri()).append(", ");
        sb.append("FileName: ").append(getFileName()).append(", ");
        sb.append("FileStatus: ").append(getFileStatus()).append(", ");
        sb.append("FileType: ").append(getFileType()).append(", ");
        sb.append("FileSize: ").append(getFileSize()).append(", ");
        return sb.toString();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFileSource() {
        return fileSource;
    }

    public void setFileSource(String fileSource) {
        this.fileSource = fileSource;
    }

    public boolean isTransferable() {
        return (getFileType() == ManagedFileType.SETUP || getFileType() == ManagedFileType.PRESCRIPTION) &&
                getFileStatus() == ManagedFileStatus.TRANSFERABLE;
    }

    public boolean isInspected() {
        return getFileType() != ManagedFileType.UNKNOWN;
    }



    public boolean isUnknownType() {
        return getFileType() == null || getFileType() == ManagedFileType.UNKNOWN;
    }

    public boolean isInvalid() {
        return ManagedFileType.INVALID.equals(getFileType());
    }

    public boolean isBackupFile() {
        return ManagedFileType.JDBACKUP.equals(getFileType());
    }

    public boolean isUploadPendingStatus() {
        return ManagedFileStatus.UPLOAD_PENDING.equals(fileStatus);
    }

    private boolean isSetupType() {
        return getFileType() == ManagedFileType.SETUP;
    }

    private boolean isDocType() {
        return getFileType() == ManagedFileType.DOC;
    }

    private boolean isSetupOrDocType() {
        return isSetupType() ||
                isDocType();
    }



}
