package com.mhelmore.service;

import com.mhelmore.domain.ManagedFile;

import java.util.List;

public interface ManagedFileService {

    ManagedFile getManageFile(Long id);

    List<ManagedFile> listUserFiles(Long userId);

}
