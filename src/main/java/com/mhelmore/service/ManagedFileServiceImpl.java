package com.mhelmore.service;

import com.mhelmore.domain.ManagedFile;
import com.mhelmore.exceptions.EntityNotFoundException;
import com.mhelmore.persistence.ManagedFileBrokerStub;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ManagedFileServiceImpl implements ManagedFileService {

    private ManagedFileBrokerStub managedFileRepository;


    @Autowired
    public ManagedFileServiceImpl(ManagedFileBrokerStub fileBrokerStub){
        this.managedFileRepository = fileBrokerStub;
    }


    @Override
    public ManagedFile getManageFile(Long id) {

        ManagedFile file = managedFileRepository.findManagedFileById(id);
        if (file == null) {
            throwEntityNotFoundException(id);
        }
        return file;
    }

    @Override
    public List<ManagedFile> listUserFiles(Long userId) {
        return managedFileRepository.listUserFiles(userId);
    }

    private ManagedFile throwEntityNotFoundException(Long id) {
        EntityNotFoundException entityNotFoundException = new EntityNotFoundException("File Doesn't Exist");
        throw entityNotFoundException;
    }


}
